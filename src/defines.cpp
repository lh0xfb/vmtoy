#include "defines.h"

const char* g_registerStrings[] = 
{
    "RIP",
    "RFLAGS",
    "RBP",
    "RSP",
    "RAX",
    "RBX",
    "RCX",
    "RDX",
    "RSI",
    "RDI",
    "R8",
    "R9",
    "R10",
    "R11",
    "R12",
    "R13",
    "R14",
    "R15",
    "XMM0",
    "XMM1",
    "XMM2",
    "XMM3",
    "XMM4",
    "XMM5",
    "XMM6",
    "XMM7",
    "NumRegisters",
};

const char* g_actionStrings[] = 
{
    "Mov",
    "Inc",
    "Dec",
    "Zer",
    "Add",
    "Sub",
    "Mul",
    "Div",
    "Mod",
    "Cmp",
    "Jmp",
    "Jz",
    "Je",
    "Jnz",
    "Jne",
    "Jg",
    "Jl",
    "Call",
    "Ret",
    "Shl",
    "Shr",
    "Not",
    "Or",
    "And",
    "Nor",
    "Nand",
    "Xor",
    "Push",
    "Pop",
    "New",
    "Delete",
    "Label",
    "NumActions",
};

const char* ActionString(const u64 action)
{
    return g_actionStrings[action];
}

const char* RegisterString(const u64 reg)
{
    return g_registerStrings[reg];
}
