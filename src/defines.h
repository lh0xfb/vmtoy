#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstdint>
#include <ctype.h>
#include <cstring>
#include <cstdlib>

typedef uint64_t u64;
typedef int64_t s64;
typedef uint32_t u32;
typedef int32_t s32;
typedef uint16_t u16;
typedef int16_t s16;
typedef uint8_t u8;
typedef int8_t s8;
typedef float f32;
typedef double f64;

inline const char* stristr(const char* a, const char* b)
{
    for(; *a; ++a)
    {
        s32 val = 0;
        size_t idx = 0;
        for(; !val && a[idx] && b[idx]; ++idx)
        {
            val = tolower(a[idx]) - tolower(b[idx]);
        }
        if(!val && !b[idx])
            return a;
    }
    return nullptr;
}

inline u64 fnv(const char* p)
{
    const u64 prime = 1099511628211;
    const u64 offset_basis = 14695981039346656037;

    u64 hash = offset_basis;
    while(*p)
    {
        hash = hash ^ (u64)tolower(*p);
        hash = hash * prime;
        ++p;
    }

    return hash;
}

struct FileStream
{
    FILE* m_stream;

    FileStream(const char* fname, const char* mode)
    {
        m_stream = fopen(fname, mode);
    }
    ~FileStream()
    {
        if(m_stream)
        {
            fclose(m_stream);
        }
    }
    operator bool () const { return m_stream != nullptr; }
    void Read(void* dest, const size_t elem_size, const size_t elem_count)
    {
        fread(dest, elem_size, elem_count, m_stream);
    }
    void Write(const void* src, const size_t elem_size, const size_t elem_count)
    {
        fwrite(src, elem_size, elem_count, m_stream);
    }
    void SeekAbs(const long pos)
    {
        fseek(m_stream, pos, SEEK_SET);
    }
    void SeekRel(const long pos)
    {
        fseek(m_stream, pos, SEEK_CUR);
    }
    void Rewind()
    {
        rewind(m_stream);
    }
    void SeekEnd()
    {
        fseek(m_stream, 0, SEEK_END);
    }
    long Position() const
    {
        return ftell(m_stream);
    }
    long Size()
    {
        const long cur_pos = Position();
        SeekEnd();
        const long sz = Position();
        SeekAbs(cur_pos);
        return sz;
    }
};

template<typename T>
struct Array
{
    T* m_data;
    u64 m_tail;
    u64 m_capacity;

    Array()
    {
        m_data = nullptr;
        m_tail = 0;
        m_capacity = 0;
    }
    ~Array()
    {
        delete[] m_data;
        m_data = nullptr;
        m_tail = 0;
        m_capacity = 0;
    }
    T& operator[](const u64 i) { return m_data[i]; }
    void Resize(const u64 new_size)
    {
        if(!new_size)
        {
            delete[] m_data;
            m_tail = 0;
            m_capacity = 0;
            return;
        }
        else
        {
            const u64 new_back = m_tail < new_size ? m_tail : new_size;
            T* new_data = new T[new_size];
            for(u64 i = 0; i < new_back; ++i)
            {
                new_data[i] = m_data[i];
            }
            delete[] m_data;
            m_data = new_data;
            m_capacity = new_size;
            m_tail = new_back;
        }
    }
    T& Grow()
    {
        if(m_tail >= m_capacity)
        {
            Resize(m_capacity << 1u ? m_capacity << 1u : 16ul);
        }
        ++m_tail;
        return m_data[m_tail - 1u];
    }
    T Pop()
    {
        --m_tail;
        return m_data[m_tail];
    }
    s64 Find(const T& item) const
    {
        for(u64 i = 0; i < m_tail; ++i)
        {
            if(m_data[i] == item)
                return (s64)i;
        }
        return -1;
    }
};


enum eRegister : u8
{
    RIP = 0,
    RFLAGS,
    RBP,
    RSP,
    RAX,
    RBX,
    RCX,
    RDX,
    RSI,
    RDI,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    XMM0,
    XMM1,
    XMM2,
    XMM3,
    XMM4,
    XMM5,
    XMM6,
    XMM7,
    NumRegisters
};

enum eAction : u8
{
    Mov,
    Inc,
    Dec,
    Zer,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Cmp,
    Jmp,
    Jz,
    Je,
    Jnz,
    Jne,
    Jg,
    Jl,
    Call,
    Ret,
    Shl,
    Shr,
    Not,
    Or,
    And,
    Nor,
    Nand,
    Xor,
    Push,
    Pop,
    New,
    Delete,
    Label,
    NumActions
};

inline bool IsJump(const u64 val)
{
    return val >= Jmp && val <= Jl;
}

enum eConfig : u64
{
    StackSize = 1024 * 10,
    HeapSize = 1024 * 10,
};

struct Instruction
{
    u64 m_value : 32;
    u64 m_action : 8;
    u64 m_dest : 8;
    u64 m_src : 8;
    u64 m_isImmediate : 8;

    Instruction()
    {
        m_value = 0;
        m_action = 0;
        m_dest = 0;
        m_src = 0;
        m_isImmediate = 0;
    }
};

struct Registers
{
    u64 m_registers[NumRegisters];
    
    Registers()
    {
        for(u64 i = 0; i < NumRegisters; ++i)
            m_registers[i] = 0;
    }
};

struct Stack
{
    u64 m_values[StackSize];
    u64 m_top = 0;
    
    Stack()
    {
        for(u64 i = 0; i < StackSize; ++i)
            m_values[i] = 0;
    }
};

struct Heap
{
    u64 m_values[HeapSize];

    Heap()
    {
        for(u64 i = 0; i < HeapSize; ++i)
            m_values[i] = 0;
    }
};

const char* ActionString(const u64 action);
const char* RegisterString(const u64 reg);

inline void Print(const Instruction inst)
{
    printf("%s | %s | %s | %s | %22llu\r\n", ActionString(inst.m_action), inst.m_isImmediate ? "imm" : "reg", RegisterString(inst.m_dest), RegisterString(inst.m_src), inst.m_value);
}

inline void Print(const Registers& regs)
{
    for(u64 i = 0; i < NumRegisters; ++i)
    {
        printf("|> %4s, %22llu <", RegisterString(i), regs.m_registers[i]);
    }
    printf("|\n");
}