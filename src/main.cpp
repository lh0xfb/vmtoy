#include "defines.h"

Registers g_registers;
Stack g_stack;
Heap g_heap;

void interpret(const Instruction* pInst, const u64 num_instructions, Registers& regs, Stack& stack, Heap& heap)
{
    while(regs.m_registers[RIP] < num_instructions)
    {
        const Instruction& inst = pInst[regs.m_registers[RIP]];

        Print(inst);
        Print(regs);

        u64& dest = regs.m_registers[inst.m_dest];
        const u64 src = inst.m_isImmediate ? inst.m_value : regs.m_registers[inst.m_src];

        switch(inst.m_action)
        {
            case Mov:
                dest = src;
                break;
            case Inc:
                ++dest;
                break;
            case Dec:
                --dest;
                break;
            case Zer:
                dest = 0;
                break;
            case Add:
                regs.m_registers[RAX] = dest + src;
                break;
            case Sub:
                regs.m_registers[RAX] = dest - src;
                break;
            case Mul:
                regs.m_registers[RAX] = dest * src;
                break;
            case Div:
                regs.m_registers[RAX] = dest / src;
                break;
            case Mod:
                regs.m_registers[RAX] = dest % src;
                break;
            case Cmp:
                regs.m_registers[RFLAGS] = dest - src;
                break;
            case Jmp:
                regs.m_registers[RIP] = inst.m_value - 1;
                break;
            case Jz:
            case Je:
                if(regs.m_registers[RFLAGS] == 0)
                {
                    regs.m_registers[RIP] = inst.m_value - 1;
                }
                break;
            case Jnz:
            case Jne:
                if(regs.m_registers[RFLAGS] != 0)
                {
                    regs.m_registers[RIP] = inst.m_value - 1;
                }
                break;
            case Jg:
                if((s64)regs.m_registers[RFLAGS] > 0)
                {
                    regs.m_registers[RIP] = inst.m_value - 1;
                }
                break;
            case Jl:
                if((s64)regs.m_registers[RFLAGS] < 0)
                {
                    regs.m_registers[RIP] = inst.m_value - 1;
                }
                break;
            case Call:
                break;
            case Ret:
                break;
            case Shl:
                regs.m_registers[RAX] = dest << src;
                break;
            case Shr:
                regs.m_registers[RAX] = dest >> src;
                break;
            case Not:
                // not rbx
                // not 5
                regs.m_registers[RAX] = ~(inst.m_isImmediate ? inst.m_value : dest);
                break;
            case Or:
                regs.m_registers[RAX] = dest | src;
                break;
            case And:
                regs.m_registers[RAX] = dest & src;
                break;
            case Nor:
                regs.m_registers[RAX] = ~(dest | src);
                break;
            case Nand:
                regs.m_registers[RAX] = ~(dest & src);
                break;
            case Xor:
                regs.m_registers[RAX] = dest ^ src;
                break;
            case Push:
                break;
            case Pop:
                break;
            case New:
                break;
            case Delete:
                break;
            case Label:
                break;
        }

        ++regs.m_registers[RIP];
    }
}

void compile_to_bytecode(FileStream& inStr, FileStream& outStr)
{
    const size_t fileSize = inStr.Size();
    char* text = new char[fileSize + 1u];
    inStr.Read(text, 1u, fileSize);
    text[fileSize] = 0;

    struct Mapping
    {
        u64 hash = 0;
        u64 value = 0;

        bool operator == (const Mapping& o) const { return hash == o.hash; }
    };

    Array<Instruction> instructions;
    Array<Mapping> action_map;
    Array<Mapping> register_map;
    Array<Mapping> labels;

    action_map.Resize(NumActions);
    for(u64 i = 0; i < NumActions; ++i)
    {
        const u64 hash = fnv(ActionString(i));
        action_map.Grow() = { hash, i };
    }
    register_map.Resize(NumRegisters);
    for(u64 i = 0; i < NumRegisters; ++i)
    {
        const u64 hash = fnv(RegisterString(i));
        register_map.Grow() = { hash, i };
    }

    const char* pos = text;
    while(pos)
    {
        const char* nextLine = strchr(pos, '\n');

        char action[64] = {0};
        char dest[64] = {0};
        char src[64] = {0};
        if(sscanf(pos, "%s %s %s", action, dest, src))
        {
            Instruction inst;
            const u64 action_hash = fnv(action);
            const u64 dest_hash = fnv(dest);
            const u64 src_hash = fnv(src);
            const s64 a = action_map.Find({action_hash, 0});
            const s64 b = register_map.Find({dest_hash, 0});
            const s64 c = register_map.Find({src_hash, 0});

            if(a != -1l)
            {
                inst.m_action = action_map[a].value;
                if(inst.m_action == Label)
                {
                    inst.m_value = dest_hash;
                    labels.Grow() = { dest_hash, instructions.m_tail };
                }
                else
                {
                    if(b != -1l)
                    {
                        inst.m_dest = register_map[b].value;
                        if(c != -1l)
                        {
                            inst.m_src = register_map[c].value;
                        }
                    }

                    if(isdigit(src[0]))
                    {
                        inst.m_value = (u64)atoll(src);
                        inst.m_isImmediate = 1u;
                    }
                    else if(isdigit(dest[0]))
                    {
                        inst.m_value = (u64)atoll(dest);
                        inst.m_isImmediate = 1u;
                    }
                    else if(IsJump(inst.m_action))
                    {
                        inst.m_value = dest_hash;
                    }
                }

                instructions.Grow() = inst;
            }
        }

        pos = nextLine ? nextLine + 1u : nullptr;
    }

    for(u64 i = 0; i < instructions.m_tail; ++i)
    {
        Instruction& inst = instructions[i];
        if(IsJump(inst.m_action))
        {
            const s64 idx = labels.Find({inst.m_value, 0});
            if(idx != -1l)
            {
                // replace hash with index of label
                inst.m_value = labels[idx].value;
            }
        }
        Print(inst);
    }

    delete[] text;

    outStr.Write(&instructions.m_tail, sizeof(u64), 1);
    outStr.Write(instructions.m_data, sizeof(Instruction), instructions.m_tail);
}

const char* check_param(const char* input, const char* key)
{
    const char* res = stristr(input, key);
    if(res)
    {
        return res + strlen(key);
    }
    return nullptr;
}

int main(int argc, const char** argv)
{
    const char* mode = nullptr;
    const char* file = nullptr;
    for(int i = 1; i < argc; ++i)
    {
        if(!mode)
        {
            mode = check_param(argv[i], "-param=");
        }

        if(!file)
        {
            file = check_param(argv[i], "-file=");
        }
        
        if(file && mode)
        {
            break;
        }
    }

    if(!mode)
    {
        puts("no mode specified, specify -mode=compile or -mode=execute");
    }
    if(!file)
    {
        puts("no file specified, specify -file=filename");
    }
    if(!mode || !file)
    {
        exit(1);
    }
    
    if(strcmp(mode, "execute") == 0)
    {
        if(FileStream stream = FileStream(file, "rb"))
        {
            u64 num_instructions = 0;
            stream.Read(&num_instructions, sizeof(u64), 1);
            if(num_instructions)
            {
                Instruction* pInst = new Instruction[num_instructions];
                stream.Read(pInst, sizeof(Instruction), num_instructions);
                interpret(pInst, num_instructions, g_registers, g_stack, g_heap);
            }
        }
    }
    else if(strcmp(mode, "compile") == 0)
    {
        if(FileStream inStream = FileStream(file, "rb"))
        {
            char buff[256] = {0};
            snprintf(buff, sizeof(buff), "%s.bytecode", file);
            if(FileStream outStream = FileStream(buff, "wb"))
            {
                compile_to_bytecode(inStream, outStream);
            }
        }
    }

    return 0;
}